"use strict";

System.register(["app/plugins/sdk", "lodash", "./rendering"], function (_export, _context) {
  "use strict";

  var MetricsPanelCtrl, _, rendering, _createClass, ChordCtrl;

  function _classCallCheck(instance, Constructor) {
    if (!(instance instanceof Constructor)) {
      throw new TypeError("Cannot call a class as a function");
    }
  }

  function _possibleConstructorReturn(self, call) {
    if (!self) {
      throw new ReferenceError("this hasn't been initialised - super() hasn't been called");
    }

    return call && (typeof call === "object" || typeof call === "function") ? call : self;
  }

  function _inherits(subClass, superClass) {
    if (typeof superClass !== "function" && superClass !== null) {
      throw new TypeError("Super expression must either be null or a function, not " + typeof superClass);
    }

    subClass.prototype = Object.create(superClass && superClass.prototype, {
      constructor: {
        value: subClass,
        enumerable: false,
        writable: true,
        configurable: true
      }
    });
    if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass;
  }

  return {
    setters: [function (_appPluginsSdk) {
      MetricsPanelCtrl = _appPluginsSdk.MetricsPanelCtrl;
    }, function (_lodash) {
      _ = _lodash.default;
    }, function (_rendering) {
      rendering = _rendering.default;
    }],
    execute: function () {
      _createClass = function () {
        function defineProperties(target, props) {
          for (var i = 0; i < props.length; i++) {
            var descriptor = props[i];
            descriptor.enumerable = descriptor.enumerable || false;
            descriptor.configurable = true;
            if ("value" in descriptor) descriptor.writable = true;
            Object.defineProperty(target, descriptor.key, descriptor);
          }
        }

        return function (Constructor, protoProps, staticProps) {
          if (protoProps) defineProperties(Constructor.prototype, protoProps);
          if (staticProps) defineProperties(Constructor, staticProps);
          return Constructor;
        };
      }();

      _export("ChordCtrl", ChordCtrl = function (_MetricsPanelCtrl) {
        _inherits(ChordCtrl, _MetricsPanelCtrl);

        function ChordCtrl($scope, $injector, $rootScope, $interpolate, $sanitize, templateSrv, detangleSrvNew) {
          _classCallCheck(this, ChordCtrl);

          var _this = _possibleConstructorReturn(this, (ChordCtrl.__proto__ || Object.getPrototypeOf(ChordCtrl)).call(this, $scope, $injector));

          _this.$rootScope = $rootScope;
          _this.$interpolate = $interpolate;
          _this.$sanitize = $sanitize;
          _this.templateSrv = templateSrv;
          _this.detangleSrv = detangleSrvNew;
          var panelDefaults = {
            detangle: {
              coupling: true,
              diamondPattern: true,
              applyFolderLevel: false,
              metric: "coupling",
              target: "file",
              cohesionCalculationMethod: "standard",
              sourceType: "$issue_type",
              targetType: "$target_issue_type",
              sourceTypeData: "",
              targetTypeData: "",
              author: "$author",
              authorData: "",
              yearData: "",
              minIssuesPerFile: "$min_issues",
              minIssuesData: "",
              minFilesPerIssue: null,
              minFilesData: "",
              issueTitle: "$issue_title",
              issueTitleData: "",
              fileExcludeFilter: "$file_exclude",
              fileExcludeFilterData: "",
              metricRange: "$metric_range",
              metricRangeData: "",
              fileGroup: "$file_group",
              threshold: 0,
              couplingPercentage: 10,
              applyDifferentCommonCondition: false,
              differentCommonConditionThreshold: 0.75
            }
          };

          _.defaults(_this.panel, panelDefaults);
          _this.panel.chordDivId = "d3chord_svg_" + _this.panel.id;
          _this.containerDivId = "container_" + _this.panel.chordDivId;
          _this.panelContainer = null;
          _this.panel.svgContainer = null;

          //this.events.on('render', this.onRender.bind(this));
          _this.events.on("data-received", _this.onDataReceived.bind(_this));
          _this.events.on("data-error", _this.onDataError.bind(_this));
          _this.events.on("data-snapshot-load", _this.onDataReceived.bind(_this));
          _this.events.on("init-edit-mode", _this.onInitEditMode.bind(_this));

          _this.couplingMetrics = [{ text: "Coupling Value", value: "coupling" }, { text: "Num. of Couples", value: "couplecounts" }, { text: "Cohesion Value", value: "cohesion" }];

          _this.targetSelections = [{ text: "Issues|Committers", value: "issues" }, { text: "Files", value: "files" }];

          _this.cohesionCalculationMethods = [{
            text: "Standard",
            value: "standard"
          }, {
            text: "Double",
            value: "double"
          }];

          _this.issueTitles = {};
          _this.issueTypes = {};
          _this.cLOCList = {};
          _this.maxCLOC = 0;
          _this.minCLOC = 0;
          return _this;
        }

        _createClass(ChordCtrl, [{
          key: "onInitEditMode",
          value: function onInitEditMode() {
            this.addEditorTab("Options", "public/plugins/grafana-chord-panel/editor.html", 2);
          }
        }, {
          key: "onDataError",
          value: function onDataError() {
            this.columnMap = [];
            this.columns = [];
            this.data = [];
            this.issueTitles = {};
            this.issueTypes = {};
            this.cLOCList = {};
            this.render();
          }
        }, {
          key: "setContainer",
          value: function setContainer(container) {
            this.panelContainer = container;
            this.panel.svgContainer = container;
          }
        }, {
          key: "colorSelectOptions",
          value: function colorSelectOptions() {
            var values = ["index", "regular expression"];

            if (!this.columns) return [];

            var selectors = _.map(this.columns, "text");

            selectors.splice(-1);

            return values.concat(selectors);
          }
        }, {
          key: "combineOptions",
          value: function combineOptions() {
            if (!this.columns || this.columns.length < 2) return [];

            return [this.columns[0].text, this.columns[1].text];
          }
        }, {
          key: "onDataReceived",
          value: function onDataReceived(dataList) {
            var data = dataList[0];

            if (!data) {
              this._error = "No data points.";
              return this.render();
            }

            if (data.type !== "table") {
              this._error = "Should be table fetch. Use terms only.";
              return this.render();
            }

            this._error = null;

            this.columnMap = data.columnMap;
            this.columns = data.columns;
            var globalTarget = this.panel.detangle.target;
            if (globalTarget) {
              this.panel.detangle.referenceCalculation = globalTarget !== "files";
              if (this.panel.detangle.referenceCalculation) {
                if (globalTarget === "issues") {
                  if (dataList.length > 1 && dataList[1].rows.length > 0) {
                    var tempColumns = dataList[1].columns;
                    var tempIssueTitleIndex = this.getIndex("@issueTitle", tempColumns);
                    if (tempIssueTitleIndex > -1) {
                      var tempIssueIdIndex = this.getIndex("@issueId", tempColumns);
                      var tempRows = dataList[1].rows;
                      var groupedIssueTitles = this.groupBy(tempRows, tempIssueIdIndex);
                      this.issueTitles = Object.keys(groupedIssueTitles).reduce(function (previous, current) {
                        previous[current + ""] = groupedIssueTitles[current][0][tempIssueTitleIndex];
                        return previous;
                      }, {});
                    }
                  }
                  if (dataList.length > 2 && dataList[2].rows.length > 0) {
                    var _tempColumns = dataList[2].columns;
                    var _tempIssueTitleIndex = this.getIndex("@issueType", _tempColumns);
                    if (_tempIssueTitleIndex > -1) {
                      var _tempIssueIdIndex = this.getIndex("@issueId", _tempColumns);
                      var _tempRows = dataList[2].rows;
                      var groupedIssueTypes = this.groupBy(_tempRows, _tempIssueIdIndex);
                      this.issueTypes = Object.keys(groupedIssueTypes).reduce(function (previous, current) {
                        previous[current + ""] = groupedIssueTypes[current][0][_tempIssueTitleIndex];
                        return previous;
                      }, {});
                    }
                  }
                }
              }
            }
            this.panel.detangle.coupling = true;
            this.panel.detangle.aggregatePeriods = true;
            this.data = this.detangleSrv.adaptDataList(dataList, this.panel.detangle, "SourceTargetPairs");
            this.formatCLOCData(dataList, this.panel.detangle.referenceCalculation, this.data);
            this.render(this.data);
          }
        }, {
          key: "formatCLOCData",
          value: function formatCLOCData(dataList, isReferenceCalculation, data) {
            var _this2 = this;

            var dataListLength = dataList.length;
            var referenceCLOCIndex = dataListLength - (isReferenceCalculation ? 2 : 1);
            var tempDatalist = dataList[referenceCLOCIndex];
            var periodicData = tempDatalist.columns.length > 2;
            var effortDataList = void 0;
            if (periodicData) {
              var effortConfig = _.clone(this.panel.detangle);
              effortConfig.coupling = false;
              effortConfig.aggregatePeriods = false;
              effortConfig.applyFolderLevel = !isReferenceCalculation;
              effortConfig.indexCalculation = true;
              effortConfig.referenceCalculation = true;
              effortConfig.isNormalized = false;
              effortConfig.multiProject = false;
              var effortDecorator = this.detangleSrv.adaptDataList([dataList[referenceCLOCIndex]], effortConfig, "Table");
              effortDataList = effortDecorator.getTable();
            } else {
              effortDataList = dataList[referenceCLOCIndex];
            }
            if (effortDataList && effortDataList.rows.length > 0) {
              var valueIndex = 1;
              var referenceIndex = 0;
              var tempRows = effortDataList.rows;
              this.maxCLOC = 0;
              this.minCLOC = effortDataList.rows[0][valueIndex];
              this.totalCLOC = 0;

              var _loop = function _loop(i) {
                var tempCurrentValue = tempRows[i][valueIndex];
                var tempReference = tempRows[i][referenceIndex];
                if (!data.some(function (x) {
                  return x.source === tempReference;
                }) && !data.some(function (x) {
                  return x.target === tempReference;
                })) {
                  return "continue";
                }
                if (tempCurrentValue > _this2.maxCLOC) {
                  _this2.maxCLOC = tempCurrentValue;
                }
                if (tempCurrentValue < _this2.minCLOC) {
                  _this2.minCLOC = tempCurrentValue;
                }
                _this2.totalCLOC += tempCurrentValue;
              };

              for (var i = 0; i < tempRows.length; i++) {
                var _ret = _loop(i);

                if (_ret === "continue") continue;
              }
              tempRows.forEach(function (rows) {
                _this2.cLOCList[rows[referenceIndex]] = rows[valueIndex];
              });
            }
          }
        }, {
          key: "link",
          value: function link(scope, elem, attrs, ctrl) {
            rendering(scope, elem, attrs, ctrl);
          }
        }, {
          key: "highlight",
          value: function highlight() {
            this.render();
          }
        }, {
          key: "getIndex",
          value: function getIndex(text, columnList) {
            return _.findIndex(columnList, { text: text });
          }
        }, {
          key: "groupBy",
          value: function groupBy(xs, key) {
            return xs.reduce(function (rv, x) {
              (rv[x[key]] = rv[x[key]] || []).push(x);
              return rv;
            }, {});
          }
        }]);

        return ChordCtrl;
      }(MetricsPanelCtrl));

      _export("ChordCtrl", ChordCtrl);

      ChordCtrl.templateUrl = "module.html";
    }
  };
});
//# sourceMappingURL=chord_ctrl.js.map

import { MetricsPanelCtrl } from "app/plugins/sdk";
import _ from "lodash";
import rendering from "./rendering";

export class ChordCtrl extends MetricsPanelCtrl {
  constructor(
    $scope,
    $injector,
    $rootScope,
    $interpolate,
    $sanitize,
    templateSrv,
    detangleSrvNew
  ) {
    super($scope, $injector);
    this.$rootScope = $rootScope;
    this.$interpolate = $interpolate;
    this.$sanitize = $sanitize;
    this.templateSrv = templateSrv;
    this.detangleSrv = detangleSrvNew;
    var panelDefaults = {
      detangle: {
        coupling: true,
        diamondPattern: true,
        applyFolderLevel: false,
        metric: "coupling",
        target: "file",
        cohesionCalculationMethod: "standard",
        sourceType: "$issue_type",
        targetType: "$target_issue_type",
        sourceTypeData: "",
        targetTypeData: "",
        author: "$author",
        authorData: "",
        yearData: "",
        minIssuesPerFile: "$min_issues",
        minIssuesData: "",
        minFilesPerIssue: null,
        minFilesData: "",
        issueTitle: "$issue_title",
        issueTitleData: "",
        fileExcludeFilter: "$file_exclude",
        fileExcludeFilterData: "",
        metricRange: "$metric_range",
        metricRangeData: "",
        fileGroup: "$file_group",
        threshold: 0,
        couplingPercentage: 10,
        applyDifferentCommonCondition: false,
        differentCommonConditionThreshold: 0.75,
      },
    };

    _.defaults(this.panel, panelDefaults);
    this.panel.chordDivId = "d3chord_svg_" + this.panel.id;
    this.containerDivId = "container_" + this.panel.chordDivId;
    this.panelContainer = null;
    this.panel.svgContainer = null;

    //this.events.on('render', this.onRender.bind(this));
    this.events.on("data-received", this.onDataReceived.bind(this));
    this.events.on("data-error", this.onDataError.bind(this));
    this.events.on("data-snapshot-load", this.onDataReceived.bind(this));
    this.events.on("init-edit-mode", this.onInitEditMode.bind(this));

    this.couplingMetrics = [
      { text: "Coupling Value", value: "coupling" },
      { text: "Num. of Couples", value: "couplecounts" },
      { text: "Cohesion Value", value: "cohesion" },
    ];

    this.targetSelections = [
      { text: "Issues|Committers", value: "issues" },
      { text: "Files", value: "files" },
    ];

    this.cohesionCalculationMethods = [
      {
        text: "Standard",
        value: "standard",
      },
      {
        text: "Double",
        value: "double",
      },
    ];

    this.issueTitles = {};
    this.issueTypes = {};
    this.cLOCList = {};
    this.maxCLOC = 0;
    this.minCLOC = 0;
  }

  onInitEditMode() {
    this.addEditorTab(
      "Options",
      "public/plugins/grafana-chord-panel/editor.html",
      2
    );
  }

  onDataError() {
    this.columnMap = [];
    this.columns = [];
    this.data = [];
    this.issueTitles = {};
    this.issueTypes = {};
    this.cLOCList = {};
    this.render();
  }

  setContainer(container) {
    this.panelContainer = container;
    this.panel.svgContainer = container;
  }

  colorSelectOptions() {
    var values = ["index", "regular expression"];

    if (!this.columns) return [];

    var selectors = _.map(this.columns, "text");

    selectors.splice(-1);

    return values.concat(selectors);
  }

  combineOptions() {
    if (!this.columns || this.columns.length < 2) return [];

    return [this.columns[0].text, this.columns[1].text];
  }

  onDataReceived(dataList) {
    let data = dataList[0];

    if (!data) {
      this._error = "No data points.";
      return this.render();
    }

    if (data.type !== "table") {
      this._error = "Should be table fetch. Use terms only.";
      return this.render();
    }

    this._error = null;

    this.columnMap = data.columnMap;
    this.columns = data.columns;
    let globalTarget = this.panel.detangle.target;
    if (globalTarget) {
      this.panel.detangle.referenceCalculation = globalTarget !== "files";
      if (this.panel.detangle.referenceCalculation) {
        if (globalTarget === "issues") {
          if (dataList.length > 1 && dataList[1].rows.length > 0) {
            let tempColumns = dataList[1].columns;
            let tempIssueTitleIndex = this.getIndex("@issueTitle", tempColumns);
            if (tempIssueTitleIndex > -1) {
              let tempIssueIdIndex = this.getIndex("@issueId", tempColumns);
              let tempRows = dataList[1].rows;
              let groupedIssueTitles = this.groupBy(tempRows, tempIssueIdIndex);
              this.issueTitles = Object.keys(groupedIssueTitles).reduce(
                function (previous, current) {
                  previous[current + ""] =
                    groupedIssueTitles[current][0][tempIssueTitleIndex];
                  return previous;
                },
                {}
              );
            }
          }
          if (dataList.length > 2 && dataList[2].rows.length > 0) {
            let tempColumns = dataList[2].columns;
            let tempIssueTitleIndex = this.getIndex("@issueType", tempColumns);
            if (tempIssueTitleIndex > -1) {
              let tempIssueIdIndex = this.getIndex("@issueId", tempColumns);
              let tempRows = dataList[2].rows;
              let groupedIssueTypes = this.groupBy(tempRows, tempIssueIdIndex);
              this.issueTypes = Object.keys(groupedIssueTypes).reduce(function (
                previous,
                current
              ) {
                previous[current + ""] =
                  groupedIssueTypes[current][0][tempIssueTitleIndex];
                return previous;
              },
              {});
            }
          }
        }
      }
    }
    this.panel.detangle.coupling = true;
    this.panel.detangle.aggregatePeriods = true;
    this.data = this.detangleSrv.adaptDataList(
      dataList,
      this.panel.detangle,
      "SourceTargetPairs"
    );
    this.formatCLOCData(
      dataList,
      this.panel.detangle.referenceCalculation,
      this.data
    );
    this.render(this.data);
  }

  formatCLOCData(dataList, isReferenceCalculation, data) {
    let dataListLength = dataList.length;
    let referenceCLOCIndex = dataListLength - (isReferenceCalculation ? 2 : 1);
    const tempDatalist = dataList[referenceCLOCIndex];
    const periodicData = tempDatalist.columns.length > 2;
    let effortDataList;
    if (periodicData) {
      const effortConfig = _.clone(this.panel.detangle);
      effortConfig.coupling = false;
      effortConfig.aggregatePeriods = false;
      effortConfig.applyFolderLevel = !isReferenceCalculation;
      effortConfig.indexCalculation = true;
      effortConfig.referenceCalculation = true;
      effortConfig.isNormalized = false;
      effortConfig.multiProject = false;
      const effortDecorator = this.detangleSrv.adaptDataList(
        [dataList[referenceCLOCIndex]],
        effortConfig,
        "Table"
      );
      effortDataList = effortDecorator.getTable();
    } else {
      effortDataList = dataList[referenceCLOCIndex];
    }
    if (
      effortDataList &&
      effortDataList.rows.length > 0
    ) {
      let valueIndex = 1;
      let referenceIndex = 0;
      var tempRows = effortDataList.rows;
      this.maxCLOC = 0;
      this.minCLOC = effortDataList.rows[0][valueIndex];
      this.totalCLOC = 0;
      for (let i = 0; i < tempRows.length; i++) {
        let tempCurrentValue = tempRows[i][valueIndex];
        let tempReference = tempRows[i][referenceIndex];
        if (
          !data.some((x) => x.source === tempReference) &&
          !data.some((x) => x.target === tempReference)
        ) {
          continue;
        }
        if (tempCurrentValue > this.maxCLOC) {
          this.maxCLOC = tempCurrentValue;
        }
        if (tempCurrentValue < this.minCLOC) {
          this.minCLOC = tempCurrentValue;
        }
        this.totalCLOC += tempCurrentValue;
      }
      tempRows.forEach(rows => {
        this.cLOCList[rows[referenceIndex]] = rows[valueIndex];
      });
    }
  }

  link(scope, elem, attrs, ctrl) {
    rendering(scope, elem, attrs, ctrl);
  }

  highlight() {
    this.render();
  }

  getIndex(text, columnList) {
    return _.findIndex(columnList, { text: text });
  }

  groupBy(xs, key) {
    return xs.reduce((rv, x) => {
      (rv[x[key]] = rv[x[key]] || []).push(x);
      return rv;
    }, {});
  }
}

ChordCtrl.templateUrl = "module.html";
